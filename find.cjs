function find(elements, cb) {
    if((Array.isArray(elements))&& (typeof cb === 'function')){
        // console.log('->')
        for(let index = 0; index < elements.length; index++){
            if(cb(elements[index])){
                return elements[index];
            }
        }
    }
}
module.exports = find;