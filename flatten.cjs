function flatten(elements,depth){
    let arr=[];
    if(depth === undefined)
    depth=1;
    for(let index=0;index<elements.length;index++){
        if(Array.isArray(elements[index])&& depth>0){
            arr = arr.concat(flatten(elements[index],depth-1));
        }
        else{
            if(elements[index]!==undefined)
            arr.push(elements[index]);
        }
    }
    return arr;
}
module.exports = flatten;