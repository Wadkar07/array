function map(elements, cb) {
    let newArr=[];
    for(let index = 0;index<elements.length;index++){
        newArr.push(cb(elements[index],index,elements));
    }
    return newArr;
}
module.exports = map;