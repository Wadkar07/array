function reduce(elements, cb, startingValue) {
    let startingPoint = 0;
    if (startingValue === undefined) {
        startingValue = elements[0];
        startingPoint = 1;
    }
    for (let index = startingPoint; index < elements.length; index++) {
        if(elements[index]!==undefined)
        startingValue = cb(startingValue, elements[index],index,elements);
    }
    return startingValue;
}
module.exports = reduce;